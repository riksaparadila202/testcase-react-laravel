<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;

class LoginController extends Controller
{
    public function loginPost(Request $request)
    {
    	$email = $request->email;
    	$password = $request->password;

    	$data = User::where('email',$email)->first();
    	if ($data) {
    		if (Hash::check($password,$data->password)) {
    			Session::put('Login',true);
    			Session::put('id',$data->id);
    			Session::put('email',$data->email);
    			Session::put('password',$data->password);
    			Session::put('name',$data->name);

    			return response()->json('berhasil');
    		}else{
    			return response()->json('gagal');
    		}
    	}else{
    		return response()->json('salah email');
    	}

    }


    public function registerPost(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
    		'password' => 'required'
    	]);

    	$data = new User;
    	$data->name = $request->name;
    	$data->email = $request->email;
    	$data->password = bcrypt($request->password);

    	$data->save();

    	return response()->json('berhasil');

    }

}
