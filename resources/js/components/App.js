import React ,{Component} from 'react';
import ReactDOm from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Login from './Login'
import Register from './Register'
import Home from './Home'

export default class App extends Component{
	render(){
		return(
            <BrowserRouter>
                <div>
                    <Switch>
                    <Route exact path='/' component={Login}/>
                    <Route exact path='/register' component={Register} />
                    <Route path='/home' component={Home} />
                    </Switch>
                </div>
            </BrowserRouter>
		);
	}
}


ReactDOm.render(<App />,document.getElementById('app'));