import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';

 
export default class Login extends Component {
     
    constructor (props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            alert: null,
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewArticle = this.handleCreateNewArticle.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }
 
    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
 
    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke Siap"
                >
                Created article successfully
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }
 
    onSuccess() {
        this.props.history.push('/home');
    }
 
    hideAlert() {
        this.setState({
            alert: null
        });
    }
 //
    handleCreateNewArticle (event) {
        event.preventDefault()
        const article = {
          email: this.state.email,
          password: this.state.password
        }
        axios.post('/api/loginPost', article).then(response => { 

                return this.goToHome();

        }).catch(error => {
          console.log(error)
        })
    }
 
    hasErrorFor (field) {
        return !!this.state.errors[field]
    }
 
    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
            <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }
 //
    render () {
        return (
        <div className='container py-4 login-container'>
              <div className='col-md-6 box'>
                <div>
                  <div><h1>Sign In</h1></div>
                  <div>
                    <form onSubmit={this.handleCreateNewArticle}>
                      <div className='form-group'>
                        <label htmlFor='email'>email</label>
                        <input
                          id='email'
                          type='text'
                          className={`form-control ${this.hasErrorFor('email') ? 'is-invalid' : ''}`}
                          name='email'
                          value={this.state.email}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('email')}
                      </div>
                      <div className='form-group'>
                        <label htmlFor='password'>password</label>
                        <input
                          id='password'
                          className={`form-control ${this.hasErrorFor('password') ? 'is-invalid' : ''}`}
                          name='password'
                          rows='10'
                          value={this.state.password}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('password')}
                      </div>

                      <button className='btn btn-primary'>Sign in</button>
                      {this.state.alert}
                      <Link
                        to={`/register`}
                        className="btnRegis">Don't have an account ?
                      </Link>                      
                    </form>
                  </div>
                </div>
              </div>

          </div>
        )
    }
}